FROM maven:3-eclipse-temurin-16 as builder

WORKDIR /opt/build
COPY src ./src
COPY pom.xml .

RUN mvn clean package
RUN mv target/$(mvn help:evaluate -Dexpression=project.artifactId -DforceStdout --non-recursive -q)-$(mvn help:evaluate -Dexpression=project.version -DforceStdout --non-recursive -q).jar ../jar.jar

WORKDIR /opt
RUN rm -rf build

FROM eclipse-temurin:18

WORKDIR /opt

COPY --from=builder /opt/jar.jar .

CMD java -jar jar.jar
