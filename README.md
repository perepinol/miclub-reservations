# miclub-reservations

Small Java application to schedule reservations for [MiClubOnline](https://miclubonline.net/).

This project fulfills the need to reserve a specific type of facility at a specific club,
and so it might not be usable if the club is changed.
However, this README will provide details on how the API works (TODO), 
so the necessary changes should be straightforward.
The aim is to gradually move as many of these club-specific settings to the configuration.