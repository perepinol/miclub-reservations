package es.perepinol.reservator;

import es.perepinol.reservator.job.ReservationJobScheduleJob;
import es.perepinol.reservator.job.ReservatorJobFactory;
import es.perepinol.reservator.resources.ReservationResource;
import es.perepinol.reservator.resources.StatusResource;
import io.undertow.Undertow;
import jakarta.ws.rs.core.Application;
import es.perepinol.reservator.job.ReservationJob;
import org.jboss.resteasy.core.ResteasyDeploymentImpl;
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App extends Application {
    @Inject
    Configuration configuration;

    @Inject
    ReservationJob reservationJob;

    @Inject
    ReservationJobScheduleJob reservationJobScheduleJob;

    @Inject
    StatusResource statusResource;

    @Inject
    ReservationResource reservationResource;

    @Inject
    App() {

    }

    @Override
    public Set<Object> getSingletons() {
        return Stream.of(
                        statusResource,
                        reservationResource
                )
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Class<?>> getClasses() {
        return Stream.of(
                ResteasyJackson2Provider.class
        ).collect(Collectors.toSet());
    }

    public static void main(String[] args) {
        App app = DaggerApplicationComponent.create().getApplication();
        Configuration.ServerConfiguration configuration = app.configuration.serverConfiguration();

        // Server
        UndertowJaxrsServer server = new UndertowJaxrsServer();

        ResteasyDeployment deployment = new ResteasyDeploymentImpl();
        deployment.setApplication(app);

        server.deploy(
                        server.undertowDeployment(deployment)
                                .setClassLoader(app.getClass().getClassLoader())
                                .setContextPath("/api")
                                .setDeploymentName("api")
                                .setSecurityDisabled(true)
                )
                .start(
                        Undertow.builder()
                                .addHttpListener(configuration.getPort(), configuration.getHost())
                );

        // Scheduler
        Scheduler scheduler;
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.setJobFactory(new ReservatorJobFactory(Stream.of(
                    app.reservationJob,
                    app.reservationJobScheduleJob
            ).collect(Collectors.toList())));
            scheduler.scheduleJob(
                    JobBuilder.newJob().ofType(ReservationJob.class)
                            .withIdentity("ReservationJobInfrequent")
                            .build(),
                    TriggerBuilder.newTrigger()
                            .withIdentity("ReservationJobTriggerInfrequent")
                            .withSchedule(CronScheduleBuilder.cronSchedule("0 */2 * * * ?"))
                            .build()
            );
            scheduler.scheduleJob(
                    JobBuilder.newJob().ofType(ReservationJobScheduleJob.class)
                            .withIdentity("SchedulerJob")
                            .build(),
                    TriggerBuilder.newTrigger()
                            .withIdentity("SchedulerJobTrigger")
                            .withSchedule(CronScheduleBuilder.cronSchedule("0 1 3 * * ?"))
                            .build()
            );

            scheduler.start();
        }
        catch (SchedulerException e) {
            throw new RuntimeException(e);
        }

        // Try to schedule the startup day's job battery since it probably has not been done
        try {
            scheduler.scheduleJob(
                    JobBuilder.newJob().ofType(ReservationJobScheduleJob.class)
                            .withIdentity("SchedulerJobOnce")
                            .build(),
                    TriggerBuilder.newTrigger()
                            .withIdentity("SchedulerJobTriggerOnce")
                            .startNow()
                            .build()
            );
        }
        catch (SchedulerException e) {
            throw new RuntimeException("Could not schedule reservation task battery", e);
        }

        // Make a single reservation run since the next will be in two minutes
        try {
            scheduler.scheduleJob(
                    JobBuilder.newJob().ofType(ReservationJob.class)
                            .withIdentity("ReservationJobOnce")
                            .build(),
                    TriggerBuilder.newTrigger()
                            .withIdentity("ReservationJobTriggerOnce")
                            .startNow()
                            .build()
            );
        }
        catch (SchedulerException e) {
            throw new RuntimeException("Could not schedule reservation task", e);
        }
    }
}
