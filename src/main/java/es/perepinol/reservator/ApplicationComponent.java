package es.perepinol.reservator;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import es.perepinol.reservator.miclub.MiClubModule;
import es.perepinol.reservator.dao.DaoModule;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        ApplicationComponent.ApplicationModule.class,
        DaoModule.class,
        MiClubModule.class
})
public interface ApplicationComponent {
    App getApplication();

    @Module
    class ApplicationModule {
        @Provides
        @Singleton
        static Configuration provideConfiguration() {
            return Configuration.getConfiguration();
        }
    }
}
