package es.perepinol.reservator;

import es.perepinol.reservator.miclub.MiClubConfiguration;
import es.perepinol.reservator.dao.DatabaseConfiguration;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public record Configuration(
        ServerConfiguration serverConfiguration,
        DatabaseConfiguration databaseConfiguration,
        MiClubConfiguration miClubConfiguration
) {
    public static Configuration getConfiguration() {
        Map<String, String> env = System.getenv();
        return new Configuration(
                new ServerConfiguration(filterMap(env, "SERVER_")),
                new DatabaseConfiguration(filterMap(env, "DATABASE_")),
                new MiClubConfiguration(filterMap(env, "MICLUB_"))
        );
    }

    private static Map<String, String> filterMap(Map<String, String> map, String startsWith) {
        return map.entrySet().stream()
                .filter(kvp -> kvp.getKey().startsWith(startsWith))
                .collect(Collectors.toMap(
                        kvp -> kvp.getKey().substring(startsWith.length()),
                        Map.Entry::getValue
                ));
    }

    public static class ServerConfiguration {
        private final String host;
        private final int port;

        public ServerConfiguration(Map<String, String> configMap) {
            this.host = Optional.ofNullable(configMap.get("HOST"))
                    .orElse("0.0.0.0");
            this.port = Optional.ofNullable(configMap.get("PORT"))
                    .map(Integer::parseInt)
                    .orElse(8080);
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }
    }
}
