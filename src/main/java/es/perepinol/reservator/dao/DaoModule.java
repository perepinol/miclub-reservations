package es.perepinol.reservator.dao;

import dagger.Module;
import dagger.Provides;
import es.perepinol.reservator.Configuration;

@Module
public class DaoModule {
    @Provides
    static DatabaseConfiguration provideDatabaseConfiguration(Configuration configuration) {
        return configuration.databaseConfiguration();
    }
}
