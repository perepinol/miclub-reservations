package es.perepinol.reservator.dao;

import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.mariadb.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class DatabaseConfiguration {
    private final String url;
    private final String username;
    private final String password;

    public DatabaseConfiguration(Map<String, String> configMap) {
        this.url = configMap.get("URL");
        this.username = configMap.get("USERNAME");
        this.password = configMap.get("PASSWORD");

        if (url == null || username == null || password == null) {
            throw new RuntimeException("Missing properties in configuration");
        }

        try {
            DriverManager.registerDriver(new Driver());
            Connection conn = DriverManager.getConnection(url, username, password);
            Liquibase liquibase = new Liquibase(
                    "changelog.xml",
                    new ClassLoaderResourceAccessor(),
                    new JdbcConnection(conn)
            );
            liquibase.update(new Contexts(), new LabelExpression());
        }
        catch (SQLException e) {
            throw new RuntimeException(String.format("Cannot connect to database %s", url), e);
        }
        catch (LiquibaseException e) {
            throw new RuntimeException("Error applying migrations", e);
        }
    }

    public DSLContext dsl() throws SQLException {
        return DSL.using(DriverManager.getConnection(url, username, password));
    }
}
