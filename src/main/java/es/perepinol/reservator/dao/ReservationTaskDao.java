package es.perepinol.reservator.dao;

import es.perepinol.reservator.db.tables.records.ReservationTaskRecord;
import es.perepinol.reservator.model.ReservationTask;
import jakarta.annotation.Nullable;
import org.jooq.Record;
import org.jooq.SelectOnConditionStep;

import javax.inject.Inject;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import static es.perepinol.reservator.db.Tables.COURT_PREFERENCE;
import static es.perepinol.reservator.db.Tables.RESERVATION_PARTICIPANTS;
import static es.perepinol.reservator.db.Tables.RESERVATION_TASK;

public class ReservationTaskDao {
    private final DatabaseConfiguration configuration;

    @Inject
    public ReservationTaskDao(DatabaseConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Gets a list of {@link ReservationTask}s.
     * @param offset The amount of tasks to skip.
     * @param limit The amount of tasks to get.
     * @return A list of {@link ReservationTask}s.
     * @throws SQLException If there is a problem fetching from the database.
     */
    public List<ReservationTask> getAll(
            @Nullable Integer offset,
            @Nullable Integer limit
    ) throws SQLException {
        var query = select();
                // TODO: readd .offset(Optional.ofNullable(offset).orElse(0));
        if (limit != null) {
            return query
                    .limit(limit)
                    .fetchStream()
                    .collect(ReservationTask.collectRecords);
        }
        return query
                .fetchStream()
                .collect(ReservationTask.collectRecords);

    }

    /**
     * Gets the {@link ReservationTask}s that have not yet been completed and are not in the past.
     * @return A list of {@link ReservationTask}s.
     * @throws SQLException If there is a problem fetching from the database.
     */
    public List<ReservationTask> getPending() throws SQLException {
        return select()
                .where(RESERVATION_TASK.COMPLETED_AT.isNull())
                .and(RESERVATION_TASK.TARGET_TIME.gt(LocalDateTime.now(ZoneOffset.UTC)))
                .fetchStream()
                .collect(ReservationTask.collectRecords);
    }

    /**
     * Creates a new {@link ReservationTask}.
     * @param reservationTask The task to create.
     * @return The created task with the new ID.
     * @throws SQLException If there is a problem creating the task.
     */
    public ReservationTask create(ReservationTask reservationTask) throws SQLException {
        Long id = configuration.dsl()
                .insertInto(RESERVATION_TASK)
                .columns(
                        RESERVATION_TASK.TARGET_TIME,
                        RESERVATION_TASK.CREATED_AT
                )
                .values(
                        reservationTask.getReservationTime(),
                        reservationTask.getCreatedAt()
                )
                .returning(RESERVATION_TASK.ID)
                .fetchOptional()
                .map(ReservationTaskRecord::getId)
                .orElse(null);

        if (id == null) {
            throw new SQLException("No changes made to database");
        }

        try {
            int success = 0;
            for (String courtId : reservationTask.getCourtIds()) {
                success += configuration.dsl()
                        .insertInto(COURT_PREFERENCE)
                        .columns(
                                COURT_PREFERENCE.RESERVATION_TASK_ID,
                                COURT_PREFERENCE.COURT_ID
                        )
                        .values(
                                id,
                                courtId
                        )
                        .execute();
            }
            if (reservationTask.getCourtIds().size() != success) {
                throw new SQLException("Could not write all court IDs");
            }
        }
        catch (SQLException e) {
            delete(id);
            throw e;
        }

        try {
            int success = 0;
            for (String participantId : reservationTask.getParticipantIds()) {
                success += configuration.dsl()
                        .insertInto(RESERVATION_PARTICIPANTS)
                        .columns(
                                RESERVATION_PARTICIPANTS.RESERVATION_TASK_ID,
                                RESERVATION_PARTICIPANTS.PARTICIPANT_ID
                        )
                        .values(
                                id,
                                participantId
                        )
                        .execute();
            }
            if (reservationTask.getParticipantIds().size() != success) {
                throw new SQLException("Could not write all participant IDs");
            }
        }
        catch (SQLException e) {
            delete(id);
            throw e;
        }
        return get(id).orElseThrow(IllegalStateException::new);
    }

    /**
     * Sets the completion time of the task with the given ID to now.
     * @param id The ID of the task to complete.
     * @throws SQLException If there is a problem updating the task.
     */
    public void complete(long id) throws SQLException {
        configuration.dsl()
                .update(RESERVATION_TASK)
                .set(RESERVATION_TASK.COMPLETED_AT, LocalDateTime.now(ZoneOffset.UTC))
                .where(RESERVATION_TASK.ID.eq(id))
                .execute();
    }

    /**
     * Deletes the task with given ID and all its information.
     * @param id The ID of the task to delete.
     * @return The deleted task.
     * @throws SQLException If there is a problem deleting the task.
     */
    public Optional<ReservationTask> delete(long id) throws SQLException {
        Optional<ReservationTask> target = get(id);
        if (target.isPresent()) {
            configuration.dsl()
                    .deleteFrom(RESERVATION_TASK)
                    .where(RESERVATION_TASK.ID.eq(id))
                    .execute();
        }
        return target;
    }

    private Optional<ReservationTask> get(long id) throws SQLException {
        return select()
                .where(RESERVATION_TASK.ID.eq(id))
                .fetchStream()
                .collect(ReservationTask.collectRecords)
                .stream()
                .findFirst();
    }

    private SelectOnConditionStep<Record> select() throws SQLException {
        return configuration.dsl()
                .select()
                .from(RESERVATION_TASK)
                .leftJoin(COURT_PREFERENCE)
                .on(RESERVATION_TASK.ID.eq(COURT_PREFERENCE.RESERVATION_TASK_ID))
                .leftJoin(RESERVATION_PARTICIPANTS)
                .on(RESERVATION_TASK.ID.eq(RESERVATION_PARTICIPANTS.RESERVATION_TASK_ID));
    }
}
