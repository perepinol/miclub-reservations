/*
 * This file is generated by jOOQ.
 */
package es.perepinol.reservator.db.tables.records;


import es.perepinol.reservator.db.tables.ReservationTask;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ReservationTaskRecord extends UpdatableRecordImpl<ReservationTaskRecord> implements Record4<Long, LocalDateTime, LocalDateTime, LocalDateTime> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>miclub.reservation_task.id</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>miclub.reservation_task.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>miclub.reservation_task.created_at</code>.
     */
    public void setCreatedAt(LocalDateTime value) {
        set(1, value);
    }

    /**
     * Getter for <code>miclub.reservation_task.created_at</code>.
     */
    public LocalDateTime getCreatedAt() {
        return (LocalDateTime) get(1);
    }

    /**
     * Setter for <code>miclub.reservation_task.target_time</code>.
     */
    public void setTargetTime(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>miclub.reservation_task.target_time</code>.
     */
    public LocalDateTime getTargetTime() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>miclub.reservation_task.completed_at</code>.
     */
    public void setCompletedAt(LocalDateTime value) {
        set(3, value);
    }

    /**
     * Getter for <code>miclub.reservation_task.completed_at</code>.
     */
    public LocalDateTime getCompletedAt() {
        return (LocalDateTime) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<Long, LocalDateTime, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<Long, LocalDateTime, LocalDateTime, LocalDateTime> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return ReservationTask.RESERVATION_TASK.ID;
    }

    @Override
    public Field<LocalDateTime> field2() {
        return ReservationTask.RESERVATION_TASK.CREATED_AT;
    }

    @Override
    public Field<LocalDateTime> field3() {
        return ReservationTask.RESERVATION_TASK.TARGET_TIME;
    }

    @Override
    public Field<LocalDateTime> field4() {
        return ReservationTask.RESERVATION_TASK.COMPLETED_AT;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public LocalDateTime component2() {
        return getCreatedAt();
    }

    @Override
    public LocalDateTime component3() {
        return getTargetTime();
    }

    @Override
    public LocalDateTime component4() {
        return getCompletedAt();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public LocalDateTime value2() {
        return getCreatedAt();
    }

    @Override
    public LocalDateTime value3() {
        return getTargetTime();
    }

    @Override
    public LocalDateTime value4() {
        return getCompletedAt();
    }

    @Override
    public ReservationTaskRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public ReservationTaskRecord value2(LocalDateTime value) {
        setCreatedAt(value);
        return this;
    }

    @Override
    public ReservationTaskRecord value3(LocalDateTime value) {
        setTargetTime(value);
        return this;
    }

    @Override
    public ReservationTaskRecord value4(LocalDateTime value) {
        setCompletedAt(value);
        return this;
    }

    @Override
    public ReservationTaskRecord values(Long value1, LocalDateTime value2, LocalDateTime value3, LocalDateTime value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ReservationTaskRecord
     */
    public ReservationTaskRecord() {
        super(ReservationTask.RESERVATION_TASK);
    }

    /**
     * Create a detached, initialised ReservationTaskRecord
     */
    public ReservationTaskRecord(Long id, LocalDateTime createdAt, LocalDateTime targetTime, LocalDateTime completedAt) {
        super(ReservationTask.RESERVATION_TASK);

        setId(id);
        setCreatedAt(createdAt);
        setTargetTime(targetTime);
        setCompletedAt(completedAt);
    }
}
