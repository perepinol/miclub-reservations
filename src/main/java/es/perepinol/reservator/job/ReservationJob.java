package es.perepinol.reservator.job;

import es.perepinol.reservator.miclub.client.MiClubClient;
import es.perepinol.reservator.dao.ReservationTaskDao;
import es.perepinol.reservator.miclub.model.Court;
import es.perepinol.reservator.model.ReservationTask;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Fetches pending reservation tasks from the database and tries to fulfill them.
 */
public class ReservationJob implements Job {
    private final Logger LOGGER = LoggerFactory.getLogger(ReservationJob.class);
    private final ReservationTaskDao reservationTaskDao;
    private final MiClubClient miClubClient;

    @Inject
    public ReservationJob(
            ReservationTaskDao reservationTaskDao,
            MiClubClient miClubClient
    ) {
        this.reservationTaskDao = reservationTaskDao;
        this.miClubClient = miClubClient;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        LOGGER.info("Starting reservation task");
        List<ReservationTask> tasks;
        try {
            tasks = reservationTaskDao.getPending();
        }
        catch (SQLException e) {
            LOGGER.error("Error fetching pending reservations", e);
            return;
        }

        if (tasks.isEmpty()) {
            LOGGER.info("No pending reservations");
        }

        tasks.forEach(task -> {
            try {
                List<Court> options = miClubClient.getCourts(task.getReservationTime().toLocalDate());
                Court target = selectCourt(options, task).orElse(null);
                if (target == null) {
                    // TODO: send email?
                    LOGGER.warn(
                            "No courts available at time {}",
                            task.getReservationTime()
                    );
                    return;
                }
                miClubClient.reserveCourt(task.getReservationTime(), target, task.getParticipantIds());
                reservationTaskDao.complete(task.getId());
                // MiClub sends an email to the participants already, so no need to do it here.
                LOGGER.info(
                        "Reserved court {} at {} successfully",
                        target,
                        task.getReservationTime()
                );

            }
            catch (IOException | SQLException e) {
                // TODO: send email?
                LOGGER.error("Failed to make reservation", e);
            }
        });
    }

    private Optional<Court> selectCourt(List<Court> courts, ReservationTask task) {
        Stream<Court> candidates;
        if (!task.getCourtIds().isEmpty()) {
            candidates = task.getCourtIds().stream()
                    .map(id -> courts.stream().filter(c -> c.id().equals(id)).findFirst())
                    .filter(Optional::isPresent)
                    .map(Optional::get);
        }
        else {
            candidates = courts.stream();
        }
        return candidates
                .filter(c -> c.isAvailable(task.getReservationTime().toLocalTime()))
                .findFirst();
    }
}
