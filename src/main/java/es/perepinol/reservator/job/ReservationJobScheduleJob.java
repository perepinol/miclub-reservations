package es.perepinol.reservator.job;

import es.perepinol.reservator.miclub.MiClubConfiguration;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

/**
 * Schedules a battery of {@link ReservationJob} at the time when reservations open in the MiClub app.
 * We cannot simply schedule a cron in UTC because the UTC time when reservations open may change
 * with DST.
 */
public class ReservationJobScheduleJob implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationJobScheduleJob.class);
    private final MiClubConfiguration miClubConfiguration;

    @Inject
    public ReservationJobScheduleJob(MiClubConfiguration miClubConfiguration) {
        this.miClubConfiguration = miClubConfiguration;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String cron = String.format(
                "*/5 0 %d * * ?",
                miClubConfiguration.getReservationOpeningUtc(LocalDate.now(ZoneOffset.UTC)).getHour()
        );
        LOGGER.info("Scheduling ReservationJob for today with the following cron: " + cron);

        try {
            context.getScheduler().scheduleJob(
                    JobBuilder.newJob().ofType(ReservationJob.class)
                            .withIdentity("ReservationJobFrequent" + LocalDate.now(ZoneOffset.UTC))
                            .build(),
                    TriggerBuilder.newTrigger()
                            .withIdentity("ReservationJobTriggerFrequent" + LocalDate.now(ZoneOffset.UTC))
                            .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                            .endAt(Date.from(Instant.now()
                                    .truncatedTo(ChronoUnit.DAYS)
                                    .plus(1, ChronoUnit.DAYS)
                            ))
                            .build()
            );
        }
        catch (SchedulerException e) {
            throw new JobExecutionException(e);
        }
    }
}
