package es.perepinol.reservator.job;

import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

import javax.inject.Inject;
import java.util.List;

public class ReservatorJobFactory implements JobFactory {
    private final List<? extends Job> jobs;

    @Inject
    public ReservatorJobFactory(List<? extends Job> jobs) {
        this.jobs = jobs;
    }

    @Override
    public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
        return jobs.stream().filter(job -> bundle.getJobDetail().getJobClass().equals(job.getClass()))
                .findFirst()
                .orElseThrow(() -> new SchedulerException("Invalid job"));
    }
}
