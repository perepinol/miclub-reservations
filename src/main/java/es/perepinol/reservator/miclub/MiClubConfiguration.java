package es.perepinol.reservator.miclub;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;

public class MiClubConfiguration {
    /**
     * The MiClub webpage host.
     */
    private final String host;

    /**
     * The MiClub authentication ID.
     */
    private final String username;

    /**
     * The MiClub authentication password.
     */
    private final String password;

    /**
     * The first time of day at which reservations are shown in the MiClub page,
     * in the webpage's timezone.
     */
    private final LocalTime dayStartLocalTime;

    /**
     * The time after which reservations can be made for tomorrow, in the webpage's timezone.
     *
     * <p>Before this time, reservations can only be made for the current day.</p>
     */
    private final LocalTime reservationOpeningLocalTime;

    /**
     * The timezone ID of the server.
     */
    private final ZoneId zoneId;

    public MiClubConfiguration(Map<String, String> configMap) {
        this.host = configMap.get("HOST");
        this.username = configMap.get("USERNAME");
        this.password = configMap.get("PASSWORD");
        this.zoneId = ZoneId.of(
                Optional.ofNullable(configMap.get("TIMEZONE"))
                        .orElse("Z")
        );
        this.dayStartLocalTime = Optional.ofNullable(configMap.get("DAY_START_HOUR"))
                .map(str -> LocalTime.of(Integer.parseInt(str), 0))
                .orElse(null);
        this.reservationOpeningLocalTime = Optional.ofNullable(configMap.get("DAY_OPEN_HOUR"))
                .map(str -> LocalTime.of(Integer.parseInt(str), 0))
                .orElse(null);

        if (host == null
                || username == null
                || password == null
                || dayStartLocalTime == null
                || reservationOpeningLocalTime == null
        ) {
            throw new RuntimeException("Missing properties in configuration");
        }
    }

    public String getHost() {
        return host;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Gets the time at which the server starts showing reservations (i.e. the time of the first character
     * in the {@link es.perepinol.reservator.miclub.model.CourtFactory}'s availability string) in UTC.
     * @param day The date at which the local time has to be converted to UTC time. Required because the
     *            server's zone ID may be in DST.
     * @return The time at which the server starts showing reservations in UTC.
     */
    public LocalTime getDayStartUtc(LocalDate day) {
        return toUtcTime(LocalDateTime.of(day, dayStartLocalTime))
                .toLocalTime();
    }

    /**
     * Gets the time after which reservations can be made for tomorrow, in UTC.
     *
     * <p>Before this time, reservations can only be made for the current day.</p>
     * @param day The date at which the local time has to be converted to UTC time. Required because the
     *            server's zone ID may be in DST.
     * @return The time at which reservations can be made for tomorrow in UTC.
     */
    public LocalTime getReservationOpeningUtc(LocalDate day) {
        return toUtcTime(LocalDateTime.of(day, reservationOpeningLocalTime))
                .toLocalTime();
    }

    /**
     * Converts the given {@link LocalDateTime} from UTC to the server's timezone.
     * @param utcTime The time to convert in UTC.
     * @return The given time in the server's timezone.
     */
    public LocalDateTime toServerTime(LocalDateTime utcTime) {
        return ZonedDateTime.of(utcTime, ZoneId.of("Z"))
                .withZoneSameLocal(ZoneId.of("Z"))
                .withZoneSameInstant(zoneId)
                .toLocalDateTime();
    }

    /**
     * Converts the given {@link LocalDateTime} from the server's timezone to UTC.
     * @param serverTime The time to convert in server time.
     * @return The given time in UTC.
     */
    public LocalDateTime toUtcTime(LocalDateTime serverTime) {
        return ZonedDateTime.of(serverTime, ZoneId.of("Z"))
                .withZoneSameLocal(zoneId)
                .withZoneSameInstant(ZoneId.of("Z"))
                .toLocalDateTime();
    }
}
