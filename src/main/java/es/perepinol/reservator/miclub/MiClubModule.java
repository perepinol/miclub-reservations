package es.perepinol.reservator.miclub;

import dagger.Module;
import dagger.Provides;
import es.perepinol.reservator.Configuration;

@Module
public class MiClubModule {
    @Provides
    MiClubConfiguration provideMiClubConfiguration(Configuration configuration) {
        return configuration.miClubConfiguration();
    }
}
