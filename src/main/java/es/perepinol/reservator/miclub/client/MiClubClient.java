package es.perepinol.reservator.miclub.client;

import es.perepinol.reservator.miclub.MiClubConfiguration;
import es.perepinol.reservator.miclub.model.Court;
import es.perepinol.reservator.miclub.model.CourtFactory;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * HTTP client to get and post information from/to the MiClub backend.
 * <p>
 * Transparently handles authentication and conversion from UTC to the MiClub server timezone,
 * as specified in the {@link MiClubConfiguration}.
 */
public class MiClubClient {
    private List<Cookie> cookies; // Should only contain login cookie
    private final MiClubConfiguration configuration;
    private final OkHttpClient client;
    private final CourtFactory courtFactory;

    @Inject
    public MiClubClient(MiClubConfiguration configuration) {
        cookies = Collections.emptyList();
        this.configuration = configuration;
        client = getClient();
        courtFactory = new CourtFactory(configuration);
    }

    /**
     * Fetches the courts IDs at the date specified by {@code date}.
     *
     * <p>Returns an empty list if it is not possible to make the reservation at the
     * date specified.</p>
     *
     * @param date The date for which courts are to be fetched.
     * @return A list of {@link Court}s.
     */
    public List<Court> getCourts(LocalDate date) throws IOException {
        // TODO: make this use the MiClub's server time just in case
        boolean canReserveAtDate = date.isBefore(LocalDate.now(ZoneOffset.UTC).plus(
                        LocalTime.now(ZoneOffset.UTC).isAfter(configuration.getReservationOpeningUtc(date))
                                ? 2
                                : 1,
                        ChronoUnit.DAYS
                )
        );
        if (!canReserveAtDate) {
            return Collections.emptyList();
        }

        // Login not required
        Request request = new Request.Builder()
                .get()
                .url(String.format(
                        "%s/infopistas/01/%s",
                        configuration.getHost(),
                        date.format(DateTimeFormatter.BASIC_ISO_DATE)
                ))
                .build();
        String bodyString;
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Fetching failed: " + response.message());
            }

            ResponseBody body = response.body();
            if (body == null) {
                throw new IOException("Null body");
            }
            bodyString = body.string();
        }

        // The court information is injected into the HTML as <script>
        Document doc = Jsoup.parse(bodyString);
        JSONArray jsonArray = doc.getElementsByTag("script")
                .stream()
                .map(script -> script.childNodes().stream().findFirst())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .map(Node::outerHtml)
                .map(js -> js.substring("jquery.extend(drupal.settings, ".length(), js.length() - ");".length()))
                .map(JSONObject::new)
                .map(obj -> obj.getJSONArray("pistas"))
                .orElse(null);

        if (jsonArray == null) return Collections.emptyList();

        List<JSONObject> jsonObjects = new LinkedList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            jsonObjects.add(jsonArray.getJSONObject(i));
        }

        return jsonObjects.stream()
                .map(obj -> courtFactory.create(
                                date,
                                (String) obj.getJSONArray("PLACECODE").get(0),
                                ((String) obj.getJSONArray("PLACEDESC").get(0)).trim(),
                                (String) obj.getJSONArray("PLACEITEM").get(0)
                        )
                )
                .collect(Collectors.toList());
    }

    /**
     * Makes a reservation at the given date and time for the given court, including the provided
     * participants.
     *
     * @param datetime The date and time in UTC at which to make the reservation. Units smaller than
     *                 or equal to minute are ignored.
     * @param court The court to reserve. Since {@link Court}s are date-specific, the court given has
     *              to match the reservation date.
     * @param participantIds A list of one to four participant IDs (usernames from MiClub). If the size
     *                       of the list is less than four, the remaining slots will be filled with
     *                       invitations.
     * @throws IOException If there is a problem during log in or reservation.
     * @throws IllegalArgumentException If the {@param datetime} and {@param court} do not have the same
     * date, or if {@param court} is not available at the requested time.
     */
    public void reserveCourt(
            LocalDateTime datetime,
            Court court,
            List<String> participantIds
    ) throws IOException {
        if (!court.isAvailable(datetime)) {
            throw new IllegalArgumentException("Court is not available at the given time");
        }
        login();

        LocalDateTime timezonedDateTime = configuration.toServerTime(datetime);
        String url = String.format(
                "%s/infopistas/01/%s/%s/%s",
                configuration.getHost(),
                timezonedDateTime.toLocalDate().format(DateTimeFormatter.BASIC_ISO_DATE),
                court.id(),
                timezonedDateTime.toLocalTime().format(DateTimeFormatter.ofPattern("HHmm"))
        );

        FormBody.Builder requestBodyBuilder = new FormBody.Builder()
                .add("privado", "1")
                .add("tiempo", "4")
                .add("reserva", "Reserva"); // Maybe this is not needed

        // 200: success; 302: failure (redirects to login)
        List<Element> hiddenInputs;
        try {
            hiddenInputs = getHiddenInputs(url, r -> r.code() == 200);
        }
        catch (IllegalStateException e) {
            throw new IOException("Not logged in");
        }
        addInputToFormBodyByName(requestBodyBuilder, hiddenInputs, "form_build_id");
        addInputToFormBodyByName(requestBodyBuilder, hiddenInputs, "form_token");
        addInputToFormBodyByName(requestBodyBuilder, hiddenInputs, "form_id");

        IntStream.range(0, 4).forEach(i -> {
            String participantId = i < participantIds.size()
                    ? participantIds.get(i)
                    : "Invitación"; // Might not be needed, but best to include it
            requestBodyBuilder.add(
                    String.format("part%d", i + 1),
                    String.format("[%s]", participantId)
            );
        });

        Request request = new Request.Builder()
                .url(url)
                .post(requestBodyBuilder.build())
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Request failed: " + response.message());
            }
        }
    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .cookieJar(new CookieJar() {
                    @Override
                    public void saveFromResponse(
                            @NotNull HttpUrl httpUrl,
                            @NotNull List<Cookie> list
                    ) {
                        if (httpUrl.uri().getPath().startsWith("/node")) {
                            cookies = list;
                        }
                    }

                    @NotNull
                    @Override
                    public List<Cookie> loadForRequest(@NotNull HttpUrl httpUrl) {
                        return cookies;
                    }
                })
                .connectTimeout(Duration.of(100, ChronoUnit.SECONDS))
                .callTimeout(Duration.of(100, ChronoUnit.SECONDS))
                .readTimeout(Duration.of(100, ChronoUnit.SECONDS))
                .build();
    }

    private void login() throws IOException {
        String url = String.format("%s%s", configuration.getHost(), "/node?destination=node");

        List<Element> hiddenInputs;
        try {
            hiddenInputs = getHiddenInputs(url, r -> r.header("Set-Cookie") != null);
        }
        catch (IllegalStateException ignored) {
            // We are already logged in, so skip the rest
            return;
        }

        FormBody.Builder formBodyBuilder = new FormBody.Builder()
                .add("op", "Entra")
                .add("name", configuration.getUsername())
                .add("pass", configuration.getPassword());
        addInputToFormBodyByName(formBodyBuilder, hiddenInputs, "form_id");
        addInputToFormBodyByName(formBodyBuilder, hiddenInputs, "form_build_id");

        Request request = new Request.Builder()
                .post(formBodyBuilder.build())
                .url(url)
                .build();

        // Cookie is set, no need to do anything else
        client.newCall(request).execute().close();

        request = new Request.Builder()
                .get()
                .url(String.format("%s%s", configuration.getHost(), "/user/info"))
                .build();
        try (Response response = client.newCall(request).execute()) {
            // 302 (Found): failure
            // 200: success
            if (response.code() != 200) {
                throw new IOException("Could not authenticate: " + response.message());
            }
        }
    }

    private void addInputToFormBodyByName(
            FormBody.Builder builder,
            List<Element> inputs,
            String name
    ) throws IOException {
        builder.add(
                name,
                inputs.stream().filter(e -> e.attr("name").equals(name))
                        .findFirst()
                        .orElseThrow(() -> new IOException("Could not find input " + name))
                        .attr("value")
        );
    }

    private List<Element> getHiddenInputs(String url, Predicate<Response> isSuccessful) throws IOException {
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();
        String bodyString;
        try (Response response = client.newCall(request).execute()) {
            if (!isSuccessful.test(response)) {
                throw new IllegalStateException("Request unsuccessful");
            }
            ResponseBody body = response.body();
            if (body == null) throw new IOException("Could not fetch form info");
            bodyString = body.string();
        }

        Document doc = Jsoup.parse(bodyString);
        return doc.getElementsByTag("input").stream()
                .filter(e -> e.attr("type").equals("hidden"))
                .collect(Collectors.toList());
    }
}
