package es.perepinol.reservator.miclub.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

public record Court(
        String id,
        String name,
        LocalDate date,
        List<LocalTime> availability
) {
    public boolean isAvailable(LocalDateTime datetime) {
        return date.equals(datetime.toLocalDate()) && isAvailable(datetime.toLocalTime());
    }

    public boolean isAvailable(LocalTime time) {
        return availability.contains(time);
    }
}
