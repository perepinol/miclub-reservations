package es.perepinol.reservator.miclub.model;

import es.perepinol.reservator.miclub.MiClubConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

/**
 * Creates {@link Court}s based on information from MiClub.
 */
public class CourtFactory {
    private final MiClubConfiguration configuration;

    public CourtFactory(MiClubConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Create a new {@link Court}, parsing the given {@param availabilityString}.
     *
     * <p>The availability string is a sequence of characters that describes the status of the court
     * during the day. There is one character for each 15 minute slot, but since reservation slots are
     * hourly, only one in four characters are required. The character describing a free slot is the
     * empty space.</p>
     *
     * <p>The time corresponding to the first character in the availability string is obtained
     * from the configuration.</p>
     *
     * @param date The date from which the {@link Court} info has been obtained.
     * @param id The ID of the court.
     * @param name The name of the court.
     * @param availabilityString The string describing the availability of the court during
     *                           the day, as obtained from MiClub.
     * @return A new instance of {@link Court}.
     */
    public Court create(
            LocalDate date,
            String id,
            String name,
            String availabilityString
    ) {

        List<LocalTime> availability = new LinkedList<>();
        for (int i = 0; i < availabilityString.length(); i += 4) {
            LocalTime time = configuration.getDayStartUtc(date).plus(i / 4, ChronoUnit.HOURS);
            if (LocalDateTime.of(date, time).isAfter(LocalDateTime.now(ZoneId.of("Z")))
                    && availabilityString.charAt(i) == ' '
            ) {
                availability.add(time);
            }
        }
        return new Court(
                id,
                name,
                date,
                availability
        );
    }
}
