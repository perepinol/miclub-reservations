package es.perepinol.reservator.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static es.perepinol.reservator.db.Tables.COURT_PREFERENCE;
import static es.perepinol.reservator.db.Tables.RESERVATION_PARTICIPANTS;
import static es.perepinol.reservator.db.Tables.RESERVATION_TASK;

@JsonSerialize
@JsonDeserialize(builder = ReservationTask.Builder.class)
public class ReservationTask {
    private final long id;
    private final List<String> courtIds;
    private final List<String> participantIds;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private final LocalDateTime reservationTime;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private final LocalDateTime createdAt;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Nullable
    private final LocalDateTime completedAt;

    private ReservationTask(
            long id,
            List<String> courtIds,
            List<String> participantIds,
            LocalDateTime reservationTime,
            LocalDateTime createdAt,
            @Nullable LocalDateTime completedAt
    ) {
        this.id = id;
        this.courtIds = courtIds;
        this.participantIds = participantIds;
        this.reservationTime = reservationTime;
        this.createdAt = createdAt;
        this.completedAt = completedAt;
    }

    public long getId() {
        return id;
    }

    public List<String> getCourtIds() {
        return courtIds;
    }

    public List<String> getParticipantIds() {
        return participantIds;
    }

    public LocalDateTime getReservationTime() {
        return reservationTime;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Nullable
    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    private static ReservationTask fromRecord(Record r) {
        return new ReservationTask.Builder()
                .setId(r.getValue(RESERVATION_TASK.ID))
                .setCourtIds(new LinkedList<>())
                .setParticipantIds(new LinkedList<>())
                .setReservationTime(r.getValue(RESERVATION_TASK.TARGET_TIME))
                .setCreatedAt(r.getValue(RESERVATION_TASK.CREATED_AT))
                .setCompletedAt(r.getValue(RESERVATION_TASK.COMPLETED_AT))
                .build();
    }

    public static Collector<Record, List<ReservationTask>, List<ReservationTask>> collectRecords = Collector.of(
            LinkedList::new,
            (list, record) -> {
                long reservationTaskId = record.getValue(RESERVATION_TASK.ID);
                String courtId = record.getValue(COURT_PREFERENCE.COURT_ID);
                String participantId = record.getValue(RESERVATION_PARTICIPANTS.PARTICIPANT_ID);
                IntStream.range(0, list.size())
                        .filter(i -> list.get(i).getId() == reservationTaskId)
                        .findFirst()
                        .ifPresentOrElse(
                                i -> {
                                    if (courtId != null && !list.get(i).courtIds.contains(courtId)) {
                                        list.get(i).courtIds.add(courtId);
                                    }
                                    if (participantId != null
                                            && !list.get(i).participantIds.contains(participantId)
                                    ) {
                                        list.get(i).participantIds.add(participantId);
                                    }
                                },
                                () -> {
                                    ReservationTask newReservation = fromRecord(record);
                                    if (courtId != null) newReservation.courtIds.add(courtId);
                                    if (participantId != null) newReservation.participantIds.add(participantId);
                                    list.add(newReservation);
                                }
                        );

            },
            (l1, l2) -> Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList()),
            list -> list.stream().distinct().collect(Collectors.toList())
    );

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationTask that = (ReservationTask) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courtIds, participantIds, reservationTime, createdAt, completedAt);
    }

    public static class Builder {
        private Long id;
        private List<String> courtIds;
        private List<String> participantIds;
        private LocalDateTime reservationTime;
        private LocalDateTime createdAt;
        private LocalDateTime completedAt;

        public Builder() {

        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setCourtIds(List<String> courtIds) {
            this.courtIds = courtIds;
            return this;
        }

        public Builder withCourtIds(List<String> courtIds) {
            return setCourtIds(courtIds);
        }

        public Builder setParticipantIds(List<String> participantIds) {
            this.participantIds = participantIds;
            return this;
        }

        public Builder withParticipantIds(List<String> participantIds) {
            return setParticipantIds(participantIds);
        }

        public Builder setReservationTime(LocalDateTime reservationTime) {
            this.reservationTime = reservationTime;
            return this;
        }

        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        public Builder withReservationTime(LocalDateTime reservationTime) {
            return setReservationTime(reservationTime);
        }

        public Builder setCreatedAt(LocalDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setCompletedAt(LocalDateTime completedAt) {
            this.completedAt = completedAt;
            return this;
        }

        public ReservationTask build() {
            if (courtIds == null
                    || participantIds == null
                    || reservationTime == null) {
                throw new IllegalArgumentException("A non-nullable field is null");
            }
            return new ReservationTask(
                    id == null ? -1 : id,
                    courtIds,
                    participantIds,
                    reservationTime,
                    createdAt == null ? LocalDateTime.now(ZoneOffset.UTC) : createdAt,
                    completedAt
            );
        }
    }
}
