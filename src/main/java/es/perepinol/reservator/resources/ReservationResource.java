package es.perepinol.reservator.resources;

import es.perepinol.reservator.dao.ReservationTaskDao;
import jakarta.annotation.Nullable;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import es.perepinol.reservator.model.ReservationTask;
import jakarta.ws.rs.core.MediaType;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

@Path("/reservation")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReservationResource {
    private final ReservationTaskDao reservationTaskDao;

    @Inject
    public ReservationResource(ReservationTaskDao reservationTaskDao) {
        this.reservationTaskDao = reservationTaskDao;
    }

    @GET
    public List<ReservationTask> getReservations(
            @Nullable @QueryParam("offset") Integer offset,
            @Nullable @QueryParam("limit") Integer limit
    ) throws SQLException {
        return reservationTaskDao.getAll(offset, limit);
    }

    @POST
    public ReservationTask createReservation(ReservationTask reservationTask) throws SQLException {
        return reservationTaskDao.create(reservationTask);
    }

    @DELETE
    @Path("/{id}")
    public ReservationTask deleteReservation(@PathParam("id") long id) throws SQLException {
        return reservationTaskDao.delete(id)
                .orElseThrow(NotFoundException::new);
    }
}
