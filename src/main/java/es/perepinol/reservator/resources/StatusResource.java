package es.perepinol.reservator.resources;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import javax.inject.Inject;

@Path("/status")
public class StatusResource {

    @Inject
    public StatusResource() {

    }

    @GET
    public Response getStatus() {
        return Response.ok().build();
    }
}
